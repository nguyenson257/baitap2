﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai8
{
    internal class PTbac2
    {
        static void Main(string[] args)
        {
            float a_146, b_146, c_146;
            Console.WriteLine("Nhap vao a : ");
            a_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao b : ");
            b_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao c : ");
            c_146 = float.Parse(Console.ReadLine());
            float delta_146 = b_146 * b_146 - 4 * a_146 * c_146;
            if (delta_146 > 0)
            {
                Console.WriteLine("Phuong trinh co hai nghiem : ");
                Console.WriteLine("X1 = {0}", ((-b_146 - Math.Sqrt(delta_146)) / 2 * a_146));
                Console.WriteLine("X2 = {0}", ((-b_146 + Math.Sqrt(delta_146)) / 2 * a_146));
            }
            else if (delta_146 == 0)
            {
                Console.WriteLine("Phuong trinh co hai nghiem kep nghiem");
                Console.WriteLine("X1 = X2 {0}", -b_146 / 2 * a_146);
            }
            else if (delta_146 < 0)
            {
                Console.WriteLine("Phuong trinh vo nghiem");
            }
        }
    }
}
