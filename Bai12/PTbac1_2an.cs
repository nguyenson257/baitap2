﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai12
{
    internal class PTbac1_2an
    {
        static void Main(string[] args)
        {
            float a1_146, b1_146, c1_146, a2_146, b2_146, c2_146, D_146, Dx_146, Dy_146, x_146, y_146; ;
            Console.WriteLine("Nhap vao a1 : ");
            a1_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao b1 : ");
            b1_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao c1 : ");
            c1_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao a2 : ");
            a2_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao b2 : ");
            b2_146 = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhap vao c2 : ");
            c2_146 = float.Parse(Console.ReadLine());

            D_146 = a1_146 * b2_146 - a2_146 * b1_146;
            Dx_146 = c1_146 * b2_146 - c2_146 * b1_146;
            Dy_146 = a1_146 * c2_146 - a2_146 * c1_146;

            if (D_146 == 0)
            {
                if (Dx_146 + Dy_146 == 0)
                    Console.WriteLine("He phuong trinh co vo so nghiem");
                else
                    Console.WriteLine("He phuong trinh vo nghiem");
            }
            else
            {
                x_146 = Dx_146 / D_146;
                y_146 = Dy_146 / D_146;
                Console.WriteLine("He phuong trinh co nghiem (x, y) = ("+ x_146 + ", "+ y_146 + ")");
            }
        }
    }
}
