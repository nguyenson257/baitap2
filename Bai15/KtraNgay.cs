﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai15
{
    internal class KtraNgay
    {
        static void Main(string[] args)
        {
            int d_146, m_146, y_146, s_146, i_146;
            Console.WriteLine("Nhap vao ngay, thang, nam");
            d_146 = Convert.ToInt16(Console.ReadLine());
            m_146 = Convert.ToInt16(Console.ReadLine());
            y_146 = Convert.ToInt16(Console.ReadLine());

            s_146 = d_146;
            for (i_146 = 1; i_146 < m_146; ++i_146)
                switch (i_146)
                {
                    case 2:
                        if (((y_146 % 4 == 0 && y_146 % 100 == 0) || y_146 % 400 == 0))
                            s_146 += 29;
                        else
                            s_146 += 28;
                        break;
                    case 4: case 6: case 9: case 11: s_146 += 30; break;
                    default: s_146 += 31; break;
                }
            Console.Write("Ngay thu: "+ s_146);
        }
    }
}
