﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai2
{
    internal class Khoangcach_AB
    {
        static void Main(string[] args)
        {
            Double xA_146, xB_146, yA_146, yB_146;
            Console.WriteLine("A(xA, yA) = ?");
            xA_146 = Convert.ToDouble(Console.ReadLine());
            yA_146 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("B(xB, yB) = ?");     
            xB_146 = Convert.ToDouble(Console.ReadLine());
            yB_146 = Convert.ToDouble(Console.ReadLine());
            Console.Write("|AB| = "+ Math.Sqrt((xB_146 - xA_146) * (xB_146 - xA_146) + (yB_146 - yA_146) * (yB_146 - yA_146)));

        }   
    }
}
