﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai25
{
    internal class UCBC
    {
        static void Main(string[] args)
        {
            int a_146, b_146, gcd_146, lcm_146;
            Console.WriteLine("Nhap vao 2 so: ");
            a_146 = Int16.Parse(Console.ReadLine());
            b_146 = Int16.Parse(Console.ReadLine());
            gcd_146 = a_146;
            while (a_146 % gcd_146 != 0 || b_146 % gcd_146 != 0) gcd_146--;
            Console.WriteLine("USCLN (a, b): "+ gcd_146);
            lcm_146 = a_146;
            while (lcm_146 % a_146 != 0 || lcm_146 % b_146 != 0) lcm_146++;
            Console.WriteLine("BSCNN (a, b): "+ lcm_146);

        }
    }
}
